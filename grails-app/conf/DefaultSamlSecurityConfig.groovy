security {
	saml {
		userAttributeMappings = [:]
		active = true
		afterLoginUrl = '/'
		afterLogoutUrl = '/'
		userGroupAttribute = "memberOf"
		responseSkew = 60
        maxAuthenticationAge = 7200
        maxAssertionTime = 3000
		autoCreate {
			active =  false
			key = 'username'
			assignAuthorities = true
		}
		metadata {
			defaultIdp = 'ping'
			url = '/saml/metadata'
			providers = [ ping :'security/idp.xml']
			sp {
				file = 'security/sp.xml'
				defaults = [
					local: true, 
					alias: 'http://4sightsolution.com/',
					securityProfile: 'metaiop',
					signingKey: '4sight',
					encryptionKey: '4sight', 
					tlsKey: '4sight',
					requireArtifactResolveSigned: false,
					requireLogoutRequestSigned: false, 
					requireLogoutResponseSigned: false ]
			}
		}
		keyManager {
			storeFile = 'classpath:security/keystore.jks'
			storePass = 'Move2Much!'
			passwords = [ "4sight": 'Move2Much!' ]
			defaultKey = 'ping'
		}
	}
}
